#!/bin/bash

export G_DB_IMAGE="gitgit-db"
export G_DB_PORT="6201"

docker build -t $G_DB_IMAGE ../db
docker stack deploy -c dev.yml --prune gitgit-dev
