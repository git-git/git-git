// @flow

import express from 'express';

const server = express();

server.disable('x-powered-by');

server.use((req, res, next) => {
  res.status(404).send({ error: 404, message: 'Not found.' });
});

export default server;
