// @flow

import server from './src/server';

const PORT = process.env.port || 6202;

server.listen(PORT, () => {
  console.log(`Server has been started on port :${PORT}`);
});
