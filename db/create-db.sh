#!/bin/bash

set -e

POSTGRES="psql --username ${POSTGRES_USER}"

echo "Creating database user";

$POSTGRES <<-EOL
CREATE USER gitgit WITH CREATEDB PASSWORD 'password';
EOL

echo "Creating database";

$POSTGRES <<-EOL
CREATE DATABASE gitgit OWNER gitgit;
EOL
