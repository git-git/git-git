# Git-git project

[![build status](https://gitlab.com/git-git/git-git/badges/master/build.svg)](https://gitlab.com/git-git/git-git/commits/master)
[![coverage report](https://gitlab.com/git-git/git-git/badges/master/coverage.svg)](https://gitlab.com/git-git/git-git/commits/master)

New web interface for git.

## Презентация
- [Идея](https://gitlab.com/git-git/git-git/blob/master/docs/presentations/idea.pptx)